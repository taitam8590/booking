jQuery(function(){
    toggleMenu();
    swtichLanguage();
    swtichStayStep();
    swtichTourType();
    useSlider();

    /* use datepicker */
    useDatepicker();
});

/* toggle main menu */
function toggleMenu(){
    jQuery(document).on("click", "#header .header-content .gnb .menu #menu-toggler", function(){
        jQuery("#header .header-content #gnb-menu").toggleClass("actived");
        jQuery("#header .header-content .gnb .book-now").toggleClass("actived");
    });
}

/* switch language */
function swtichLanguage() {
    jQuery(document).on("click", "#header .header-content .gnb .gnb-right .lang-switcher ul > li", function () {
        jQuery("#header .header-content .gnb .gnb-right .lang-switcher ul > li").removeClass("actived");
        jQuery(this).toggleClass("actived");
    });
}

/* switch stay steps */
function swtichStayStep() {
    jQuery(document).on("click", "#menu-stay-step li button", function () {
        let li = jQuery(this).closest("li");
        let idx = jQuery(li).index();
        jQuery("#menu-stay-step li button").removeClass("actived");
        jQuery("#menu-stay-step li").eq(idx).find("button").addClass("actived");
        jQuery("#content-stay-step ul").hide();
        jQuery("#content-stay-step ul").eq(idx).show();
    });
}

/* switch tour type */
function swtichTourType() {
    jQuery(document).on("click", ".look-for-tour .search-tour .tour-type > li button", function () {
        jQuery(".look-for-tour .search-tour .tour-type > li button").removeClass("actived");
        jQuery(this).addClass("actived");
    });
}

/* use slider */
function useSlider() {
    jQuery("#tour-info-slider").slick({
        dots: true,
        arrows: false,
    });
};

/* use datepicker */
function useDatepicker() {
    jQuery(".date-picker").datepicker();
}
